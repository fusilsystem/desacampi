#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2002-11-01 04:27+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: FSerialImpresion.form:26
msgid "Serial Port"
msgstr ""

#: FSerialImpresion.form:30
msgid "Impresora (epson V220)"
msgstr ""

#: FSerialImpresion.form:35
msgid "/dev/ttyS0"
msgstr ""

#: FSerialImpresion.form:40
msgid "Open"
msgstr ""

#: FSerialImpresion.form:50
msgid "Write data then press \"CR\""
msgstr ""

#: FSerialImpresion.form:56
msgid "CR"
msgstr ""

#: FSerialImpresion.form:65
msgid "DSR"
msgstr ""

#: FSerialImpresion.form:70
msgid "DTR"
msgstr ""

#: FSerialImpresion.form:75
msgid "RTS"
msgstr ""

#: FSerialImpresion.form:80
msgid "CTS"
msgstr ""

#: FSerialImpresion.form:85
msgid "DCD"
msgstr ""

#: FSerialImpresion.form:90
msgid "RNG"
msgstr ""

#: FSerialImpresion.form:98
msgid "Panaderia"
msgstr ""

#: FSerialImpresion.form:103
msgid "Empanadas (con grasa)"
msgstr ""

#: FSerialImpresion.form:108
msgid "Arepas (jum)"
msgstr ""

#: FSerialImpresion.form:113
msgid "Menu del dia (solo entre 8:00 y 8:45) no se exceda"
msgstr ""

#: .project:1
msgid "Impresion campitos"
msgstr ""

#: .project:2
msgid ""
"En esta recrudecida ultra crisis hay que cuidar la salud... del bolsillo"
msgstr ""
